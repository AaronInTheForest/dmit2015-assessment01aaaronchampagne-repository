package ca.nait.dmit.domain;

import static org.junit.Assert.*;

import org.junit.Test;

public class HouseRulesTest {

	@Test
	public void testMaximumMonthlyPaymentLowDebt() {
		HouseRules houseRules = new HouseRules();
		assertEquals(1300.0, houseRules.maximumMonthlyPayment(5000.0, 500.0),1);
	}
	@Test
	public void testMaximumMonthlyPaymentHighDebt() {
		HouseRules houseRules = new HouseRules();
		assertEquals(300.0, houseRules.maximumMonthlyPayment(5000.0, 1500.0),1);
	}
	@Test
	public void testMaximumMonthlyPaymentNoDebt() {
		HouseRules houseRules = new HouseRules();
		assertEquals(1400.0, houseRules.maximumMonthlyPayment(5000.0, 0.0),1);//pass
	}
}
