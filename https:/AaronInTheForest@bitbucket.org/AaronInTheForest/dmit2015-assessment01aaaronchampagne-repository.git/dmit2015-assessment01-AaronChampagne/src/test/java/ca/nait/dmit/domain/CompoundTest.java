package ca.nait.dmit.domain;

import static org.junit.Assert.*;

import org.junit.Test;

public class CompoundTest {

	@Test
	public void testCase1() {
		CompoundInterest compoundInterest = new CompoundInterest(5000.0,0.036,5,1);
		
		assertEquals(5967.18,compoundInterest.futureValue(),1);
		assertEquals(967.18,compoundInterest.interestEarned(),1);
	}
	@Test
	public void testCase2() {
		CompoundInterest compoundInterest = new CompoundInterest(5000.0,0.036,5,2);
		
		assertEquals(5976.51,compoundInterest.futureValue(),1);
		assertEquals(976.51,compoundInterest.interestEarned(),1);
	}
	@Test
	public void testCase3() {
		CompoundInterest compoundInterest = new CompoundInterest(5000.0,0.036,5,4);
		
		assertEquals(5981.27,compoundInterest.futureValue(),1);
		assertEquals(981.27,compoundInterest.interestEarned(),1);
	}
	@Test
	public void testCase4() {
		CompoundInterest compoundInterest = new CompoundInterest(5000.0,0.036,5,12);
		
		assertEquals(5984.47,compoundInterest.futureValue(),1);
		assertEquals(984.47,compoundInterest.interestEarned(),1);
	}
	

}
