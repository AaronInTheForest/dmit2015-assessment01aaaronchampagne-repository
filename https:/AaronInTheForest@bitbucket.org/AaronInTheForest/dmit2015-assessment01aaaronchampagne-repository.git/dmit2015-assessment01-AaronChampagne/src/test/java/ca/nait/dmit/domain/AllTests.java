package ca.nait.dmit.domain;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ CompoundTest.class, HouseRulesTest.class })
public class AllTests {

}
