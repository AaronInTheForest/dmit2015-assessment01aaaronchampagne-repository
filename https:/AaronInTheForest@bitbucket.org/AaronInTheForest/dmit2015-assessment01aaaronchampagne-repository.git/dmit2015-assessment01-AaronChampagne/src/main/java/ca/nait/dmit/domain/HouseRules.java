package ca.nait.dmit.domain;
/**
 * This class is used to calculate a person's maximum monthly mortgage payment based on monthly income and monthly debit payments.
 * @author achampagne2
 * @version 2015.02.09
 */
public class HouseRules {
	
	public HouseRules(){
		super();
	}
	/**
	 * Calculate a person's maximum monthly mortgage payment based on monthly income and monthly debit payments.
	 * @param monthlyIncome
	 * @param monthlyDebitPayments
	 * @return Maximum monthly payment based on one of two formulas (monthly income * 0.28) (monthly income * 0.36 - monthly debit payments).
	 */
	public double maximumMonthlyPayment(double monthlyIncome,
			double monthlyDebitPayments) {
		double maximumMonthlyPayment;
		if (monthlyDebitPayments == 0) {
			//pass
			maximumMonthlyPayment = monthlyIncome * 0.28;
		} else {
			//fail...
			maximumMonthlyPayment = ((monthlyIncome * 0.36) - monthlyDebitPayments)*1.0 ;
		}
		return maximumMonthlyPayment;
	}
}
