package ca.nait.dmit.domain;
/**
 * This class is used to calculate the compound interest on a mortgage based on principal, interest, length of mortgage, and number of compounds.
 * @author achampagne2
 * @version 2015.02.09
 */
import org.junit.runner.Computer;

public class CompoundInterest {
	double principalAmount;
	double annualInterestRate;
	int years;
	int compounds;
	
	public CompoundInterest() {
		super();
	}
	/**
	 * Calculate the compound interest on a mortgage based on principal, interest, length of mortgage, and number of compounds.
	 * @param principal
	 * @param annualInterestRate
	 * @param years
	 * @param compounds
	 */
	public CompoundInterest(double principal, double annualInterestRate, int years, int compounds) {
		this.principalAmount = principal;
		this.annualInterestRate = annualInterestRate;
		this.years = years;
		this.compounds = compounds;
	}
	/**
	 * Calculates the future value of a mortgage
	 * @return double representing the final value of a mortgage
	 */
	public double futureValue(){
		double futureValue;
		
		futureValue = principalAmount * Math.pow(1+(annualInterestRate/compounds),(compounds*years));
		
		return futureValue;
	}
	/**
	 * Calculates the total interest earned by the lending agency
	 * @return double representing the total interest earned based on future value - principal
	 */
	public double interestEarned(){
		double interestEarned;
		interestEarned = this.futureValue() - principalAmount;
		return interestEarned;
	}

}
