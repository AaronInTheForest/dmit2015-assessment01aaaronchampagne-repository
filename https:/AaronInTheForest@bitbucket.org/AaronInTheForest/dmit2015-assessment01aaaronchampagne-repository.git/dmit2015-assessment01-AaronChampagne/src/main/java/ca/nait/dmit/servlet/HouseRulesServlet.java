package ca.nait.dmit.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ca.nait.dmit.domain.HouseRules;
/**
 * Servlet implementation class HouseRulesServlet
 */
@WebServlet("/HouseRulesServlet")
public class HouseRulesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public HouseRulesServlet() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String monthlyIncomeString = request.getParameter("monthlyIncomeTextBox");
		String monthlyDebitPaymentsString = request.getParameter("monthlyDebitPaymentsTextBox");
		
		if(monthlyDebitPaymentsString.isEmpty() || monthlyIncomeString.isEmpty()){
			response.setContentType("text/html");
			response.getWriter().println(
					"Please input a value for both monthly income and monthly debit payments.");
			
		}else{
			try{
			double monthlyIncome = Double.parseDouble(monthlyIncomeString);
			double monthlyDebitPayments = Double.parseDouble(monthlyDebitPaymentsString);
			double maxPayment;
			
			HouseRules houseRulesBean = new HouseRules();
			maxPayment = houseRulesBean.maximumMonthlyPayment(monthlyIncome, monthlyDebitPayments);
			
			response.setContentType("text/html");
			response.getWriter().println(
					"Your Maximum Monthly Payment is, <strong>" + maxPayment
							+ ".");
			}catch(Exception e){
				response.setContentType("text/html");
				response.getWriter().println(
						"<strong>Please input numeric values only. \nError:" + e.getMessage()
								+ "</strong>");
			}
		}
	}

}
