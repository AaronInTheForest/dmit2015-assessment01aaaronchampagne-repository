<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="ca.nait.dmit.domain.CompoundInterest, java.text.NumberFormat"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Compund Interest Calculator</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" />	
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css" />
<script src="http://code.jquery.com/jquery-2.1.3.min.js"></script>	
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="jumbotron">
		<h1>Compound Interest Calculator</h1>
	</div>
	<% 
	if(request.getMethod().equalsIgnoreCase("POST")){
		String principalAmountString = request.getParameter("principalAmountTextBox");
		String interestRateString = request.getParameter("interestRateTextBox");
		String yearsString = request.getParameter("yearsTextBox");
		String compoundsString = request.getParameter("compounds");
		if (principalAmountString.isEmpty()||interestRateString.isEmpty()||yearsString.isEmpty()||compoundsString.isEmpty()) {
			//response.setContentType("text/html");
			out.println("Please input all values.");
		} else {

			try {
				double principalAmount = Double.parseDouble(principalAmountString);
				double interestRate = Double.parseDouble(interestRateString);
				int years = Integer.parseInt(yearsString);
				int compounds = Integer.parseInt(compoundsString);

				CompoundInterest compoundInterestBean = new CompoundInterest(principalAmount,interestRate,years,compounds);

				double futureValue = compoundInterestBean.futureValue();
				double interestEarned = compoundInterestBean.interestEarned();

				//response.setContentType("text/html");
				out.println(
						"Your future value is <strong>" + futureValue
								+ "</strong>, and your interest earned is <strong>" + interestEarned
								+ ".</strong>");
			} catch (Exception e) {
				// find a way to output this message to html
				//response.setContentType("text/html");
				out.println(
						"<strong>Please input numeric values only. \nError:"
								+ e.getMessage() + "</strong>");
			}
		}
	}
	%>
	
	
	<form method="post" action="CompoundInterestServlet">
		<label id="principalAmount">Principal Amount:</label>
		<br/>
		<input type="text" name="principalAmountTextBox" placeholder="enter your mortgage principal"/>
		<br/>
		<label id="interestRate">Interest Rate:</label>
		<br/>
		<input type="text" name="interestRateTextBox" placeholder="enter your interest rate"/>
		<br/>
		<label id="years">Years:</label>
		<br/>
		<input type="text" name="yearsTextBox" placeholder="enter the length of you mortgage in years"/>
		<br/>
		
		<h:panelGrid columns="2" style="margin-bottom:10px" cellpadding="5">
        <p:outputLabel for="compounds" value="Compounds:" />
        <p:selectOneRadio id="compounds" value="#{radioView.console}">
            <f:selectItem itemLabel="Yearly" itemValue="1" />
            <f:selectItem itemLabel="Semi-Annualy" itemValue="2" />
            <f:selectItem itemLabel="Quarterly" itemValue="4" />
            <f:selectItem itemLabel="Monthly" itemValue="12" />
        </p:selectOneRadio>
    	</h:panelGrid>
		<br/>
		<input type="submit" id="submit" value="Calculate"/>
	</form>
</body>
</html>